const http = require('http');
const database = require('./src/database')

const express = require('express')
const app = express()

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');

  const movie = {
    title: "Hier de titel",
    lijst: [
      { test: "Hier een tekst"}
    ]
  };

  database.movies.push(movie);

  res.end(JSON.stringify(database));
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

// app.listen(port, () => console.log(`Example app listening on port ${port}!`))
//
// app.post('/', function (req, res) {
//   res.send('Got a POST request')
// })
//
// app.put('/user', function (req, res) {
//   res.send('Got a PUT request at /user')
// })
//
// app.delete('/user', function (req, res) {
//   res.send('Got a DELETE request at /user')
// })
//
// app.use(function (req, res, next) {
//   res.status(404).send("Sorry can't find that!")
// })
//
// app.use(function (err, req, res, next) {
//   console.error(err.stack)
//   res.status(500).send('Something broke!')
// })
